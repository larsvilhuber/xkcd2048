
xo = function(){
    list(
    col = "red",
    lwd = 5,
    xrange = seq(min(xkcd$x), max(xkcd$x), len=100)
    )
}

baseplot <- function(name){
    plot(xkcd$x, xkcd$y, pch=19, cex=2, bty="n", axes=FALSE, xlab="", ylab="")
    box(bty="L", col="grey", lwd=5)
    text(min(xkcd$x), 9,name, cex=3, adj=0, col="grey", xpd=NA)
}

ggbaseplot <- function(name){
    ggplot(xkcd, aes(x=x,y=y)) + geom_point(size=3) + xkcd::theme_xkcd() +
        theme(axis.text=element_blank(), axis.title=element_blank(),
              axis.ticks=element_line(size=0)) +
        xkcdaxis(c(0,10),c(0,10),size=3) +
        geom_text(x=0.2,y=9.5,label=name,size=10, hjust=0, vjust=1, family="xkcd", col="grey",lineheight=0.9)
}

xkline <- function(d,lwd=2,col="red"){
    geom_line(data=d, aes(x=x,y=y), col=col,lwd=lwd)
}

###
### xkcd 2048 fits
###

### 1. linear

linear <- function(){
    m = lm(y~x, data=xkcd)
    d = data.frame(x=xkcd$x, y=predict(m))
    ggbaseplot("LINEAR") + 
        xkline(d)
}

### 2. quadratic

quadratic <- function(){
    m = lm(y~poly(x,2), data=xkcd)
    f = predict(m, newdata = data.frame(x=xo()$xrange))
    nd = data.frame(x=xo()$xrange, y=f)
    ggbaseplot("QUADRATIC") + 
        xkline(nd)
}

### 3. logarithmic

logarithmic <- function(){
    logp = function(x, b1, b2){
        b1*log(b2*x)
    }
    m = nls(y~logp(x,a,b), data=xkcd, start=list(a=3, b=2))
    f = predict(m, newdata = data.frame(x=xo()$xrange))
    d = data.frame(x=xo()$xrange, y=f)
    ggbaseplot("LOGARITHMIC") +
        xkline(d)

}


### 4. exponential

exponential <- function(){
    expp = function(x, b0, b1, b2){
        b0+b1*exp(b2*x)
    }
    m = nls(y~expp(x,a,b,c ), data=xkcd, start=list(a=0, b=3, c=.2 ))
    f = predict(m, newdata = data.frame(x=xo()$xrange))
    ggbaseplot("EXPONENTIAL") + 
        xkline(data.frame(x=xo()$xrange, y=f))
}

### 5. loess

loess_plot <- function(){
    m = loess(y~x, data=xkcd)
    f = predict(m, xo()$xrange)
    ggbaseplot("LOESS") + 
        xkline(data.frame(x=xo()$xrange, y=f))
}

### 6. linear no slope

linear_no_slope <- function(){
    ggbaseplot("LINEAR,\nNO SLOPE") + 
        xkline(data.frame(x=range(xkcd$x),y=rep(mean(xkcd$y),2)))
}

### 7. logistic

logistic <- function(){
    logitp = function(x, p0, p1, p2, p3){
        t = p2 + p3*x
        p0 + p1 * (1/(1+exp(-t)))
    }
    m =nls(y~logitp(x, a, b, c, d), data=xkcd, start=list(a=2, b=5, c=-5, d=1))
    f = predict(m, newdata=data.frame(x=xo()$xrange))
    ggbaseplot("LOGISTIC") +
        xkline(data.frame(x=xo()$xrange, y=f))
    
}

### 8. confidence interval
confidence <- function(){
    ggbaseplot("CONFIDENCE\nINTERVAL\ntodo")
}

### 9. piecewise

piecewise <- function(){
    d1 = xkcd[1:(nrow(xkcd)/2),]
    d2 = xkcd[-(1:(nrow(xkcd)/2)),]
    part <- function(d){
        n1 = data.frame(x=seq(min(d$x),max(d$x),len=100))
        m1 = lm(y~x, data=d)
        f1 = predict(m1, newdata=n1, se.fit=TRUE)
        list(xkline(data.frame(x=d$x, y=m1$fit)),
             xkline(data.frame(x=n1$x, y=f1$fit + f1$se.fit*2),lwd=0.5),
             xkline(data.frame(x=n1$x, y=f1$fit - f1$se.fit*2),lwd=0.5)
             )
    }
    p1 = part(d1)
    p2 = part(d2)
    ggbaseplot("PIECEWISE") + 
        p1[[1]] + p1[[2]] + p1[[3]] + 
        p2[[1]] + p2[[2]] + p2[[3]]
}


### 10. connecting lines

connecting_lines <- function(){
    ggbaseplot("CONNECTING\nLINES") + 
    xkline(data.frame(x=xkcd$x,y= xkcd$y))
}


### 11. ad-hoc filter
ad_hoc <- function(){
    ## running median looks close
    ym = runmed(xkcd$y,5)
    ggbaseplot("AD-HOC") + 
        xkline(data.frame(x=xkcd$x, y=ym))
}

### 12. house of cards
house_of_cards <- function(){
    ggbaseplot("HOUSE OF\nCARDS\ntodo")
}

